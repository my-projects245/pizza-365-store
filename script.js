'use strict'
$(document).ready(function(){

    // Region 1 - biến toàn cục
    var gSelectedMenuStructure = null;

    var gSelectedPizzaType = null;

    var gVoucher = {
        voucher : '',
        giaDiscount : -1,
        phanTramGiamGia : -1

    };
    // Region 2 - gán sự kiện
    onPageLoading()
    // gán sự kiện btn chọn size click
    $('#btn-size-s').on('click', onBtnSizeSClick);
    $('#btn-size-m').on('click', onBtnSizeMClick);
    $('#btn-size-l').on('click', onBtnSizeLClick);
    // gán sụe kiện btn chọn loại pizza
    $('#btn-ocean').on('click', onBtnOceanClick);
    $('#btn-hawaii').on('click', onBtnHawaiiClick);
    $('#btn-bacon').on('click', onBtnBaconClick);
    // gán sự kiện btn gửi order
    $('#btn-sendorder').on('click', function(){
        onBtnSendOrderClick()
    })
    // gán sự kiện btn tạo đơn
    $('#btn-createorder').on('click', function(){
        onBtnCreateOrderClick()
    })

    // Region 3 - hàm xử lý sự kiện
    function onPageLoading(){
        console.log('%c=== Page Loading ===', 'color : yellow');
        loadListDrinkToSelect()
    };
    // hàm xư lý sự kiện các nút chọn size pizza click
    // khi click nút được click đổi màu với các nút còn lại
    // lưu thông tin được chọn vào gSelectedMenuStructure
    function onBtnSizeSClick(){
        var vMenuStructure = {};

        vMenuStructure.kichCo = 'S';
        vMenuStructure.duongKinh = '20cm';
        vMenuStructure.suon = 2;
        vMenuStructure.salad = '200g';
        vMenuStructure.soLuongNuoc = 2;
        vMenuStructure.thanhTien = 150000;

        gSelectedMenuStructure = vMenuStructure;

        console.log(gSelectedMenuStructure);
        changeColorBtnClick('small');
    };
    function onBtnSizeMClick(){
        var vMenuStructure = {};

        vMenuStructure.kichCo = 'M';
        vMenuStructure.duongKinh = '25cm';
        vMenuStructure.suon = 4;
        vMenuStructure.salad = '300g';
        vMenuStructure.soLuongNuoc = 3;
        vMenuStructure.thanhTien = 200000;

        gSelectedMenuStructure = vMenuStructure;
        
        console.log(gSelectedMenuStructure);
        changeColorBtnClick('medium');
    };
    function onBtnSizeLClick(){
        var vMenuStructure = {};
        vMenuStructure.kichCo = 'L';
        vMenuStructure.duongKinh = '30cm';
        vMenuStructure.suon = 8;
        vMenuStructure.salad = '500g';
        vMenuStructure.soLuongNuoc = 4;
        vMenuStructure.thanhTien = 250000;

        gSelectedMenuStructure = vMenuStructure;

        console.log(gSelectedMenuStructure);
        changeColorBtnClick('large');
    };
    // hàm xư lý sự kiện các nút chọn loại pizza click
    // khi click nút được click đổi màu với các nút còn lại
    // lưu thông tin được chọn vào gSelectedPizzaType
    function onBtnOceanClick(){
        var vPizzaType = {};

        vPizzaType.loaiPizza = 'Ocean';

        gSelectedPizzaType = vPizzaType

        console.log(gSelectedPizzaType);
        changeColorBtnClick('ocean');
    };
    function onBtnHawaiiClick(){

        var vPizzaType = {};

        vPizzaType.loaiPizza = 'Hawaii';

        gSelectedPizzaType = vPizzaType
       
        console.log(gSelectedPizzaType);
        changeColorBtnClick('hawaii');
    };
    function onBtnBaconClick(){
        var vPizzaType = {};

        vPizzaType.loaiPizza = 'Bacon';

        gSelectedPizzaType = vPizzaType

        console.log(gSelectedPizzaType);
        changeColorBtnClick('bacon');
    };

    // hàm xử lý sự kiện gửi click - lấy thông tin từ form load vào modal
    function onBtnSendOrderClick(){
        var vOrderObj = {
            menuStructure : null,
            pizzaType : null,
            idLoaiNuocUong : '',
            hoTen : '',
            soDienThoai : '',
            email : '',
            diaChi : '',
            idVourcher : '',
            loiNhan : '',
        }

        getDataOrderObj(vOrderObj)

        var vChecked = validataOrderObj(vOrderObj)

        findDiscountByVoucherId(vOrderObj)

        if(vChecked){
            $('#modal-createorder').modal('show')
            loadDataFormModalCreateOrder(vOrderObj, gVoucher)
        }
    }

    // hàm xử lý sự kiện tạo đơn được click
    function onBtnCreateOrderClick(){
        console.log('click')
        $('#modal-createorder').modal('hide')
        var vObjectRequest = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            thanhTien: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: ""
        }
        getOrderObjRequest(vObjectRequest)
        
        const vBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
        $.ajax({
            url : vBASE_URL,
            type : 'post',
            contentType : "application/json;charset=UTF-8",
            data : JSON.stringify(vObjectRequest),
            success : function(res){
                var vOrderID = res.orderId
                console.log(vOrderID)
                $('#modal-createorder-alert').modal('show')
                $('#p-orderid').html(vOrderID)
                console.log(vObjectRequest)
            }
        })
        
    }

    // Region 4 - hàm dùng chung

    // hàm load thông tin đơn hàng lên form modal create order
    function loadDataFormModalCreateOrder(paramOrderObj, paramVoucher){
        $('#p-name-modal-order').html(paramOrderObj.hoTen)
        $('#p-phone-modal-order').html(paramOrderObj.soDienThoai)
        $('#p-address-modal-order').html(paramOrderObj.diaChi)
        $('#p-message-modal-order').html(paramOrderObj.loiNhan)
        if(paramVoucher.phanTramGiamGia != 0){
            $('#p-voucherid-modal-order').html(paramOrderObj.idVourcher)
        }else{
            $('#p-voucherid-modal-order').html('')
        };
        
        $('#p-datail-modal-order').html(
            'Xác nhận: ' + paramOrderObj.hoTen + ', ' + paramOrderObj.soDienThoai + ', ' + paramOrderObj.diaChi + '<br>'
            + 'Menu: ' + paramOrderObj.menuStructure.kichCo + ', Sườn nướng: ' + paramOrderObj.menuStructure.suon + ', Nước: ' + paramOrderObj.menuStructure.soLuongNuoc + ', Salad: ' + paramOrderObj.menuStructure.salad
            + '<br> Loại Pizza : ' + paramOrderObj.pizzaType.loaiPizza + ', Giá: ' + paramOrderObj.menuStructure.thanhTien + ' vnd' + ', Mã giảm giá: ' + paramOrderObj.idVourcher
            + '<br> Phải thanh toán: ' + paramVoucher.giaDiscount + ' vnd ' + '(giảm giá ' + paramVoucher.phanTramGiamGia + '%)'
            )
    }

    // hàm gọi api check mã voucher
    function findDiscountByVoucherId(paramOrderObj){
        if(paramOrderObj.idVourcher === ''){
            gVoucher.phanTramGiamGia = 0;
            gVoucher.giaDiscount = paramOrderObj.menuStructure.thanhTien
        }else{
            const vUrlVoucher = "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/";
            $.ajax({
                url : vUrlVoucher + paramOrderObj.idVourcher,
                type : 'get',
                dataType : 'json',
                async : false,
                success : function(res){
                    gVoucher.voucher = res;
                    gVoucher.phanTramGiamGia = res.phanTramGiamGia;
                    gVoucher.giaDiscount = paramOrderObj.menuStructure.thanhTien * ( 100 - res.phanTramGiamGia)/100 // tính phần trăm giảm giá 
                },
                error : function(){
                    gVoucher.phanTramGiamGia = 0;
                    gVoucher.giaDiscount = paramOrderObj.menuStructure.thanhTien;
                    alert('Mã Voucher Không Tồn Tại');
                }
            })
        }
    }

    // hàm tổng hợp dữ liệu order
    function getOrderObjRequest(paramOrderRequest){
        paramOrderRequest.kichCo = gSelectedMenuStructure.kichCo
        paramOrderRequest.duongKinh = gSelectedMenuStructure.duongKinh
        paramOrderRequest.suon = gSelectedMenuStructure.suon
        paramOrderRequest.salad = gSelectedMenuStructure.salad
        paramOrderRequest.loaiPizza = gSelectedPizzaType.loaiPizza
        paramOrderRequest.idVourcher = $('#p-voucherid-modal-order').html()
        paramOrderRequest.idLoaiNuocUong = $('#sel-drinks option:selected').val()
        paramOrderRequest.soLuongNuoc = gSelectedMenuStructure.soLuongNuoc
        paramOrderRequest.hoTen = $('#inp-name').val()
        paramOrderRequest.thanhTien = gVoucher.giaDiscount
        paramOrderRequest.email = $('#inp-email').val()
        paramOrderRequest.soDienThoai = $('#inp-phone').val()
        paramOrderRequest.diaChi = $('#inp-address').val()
        paramOrderRequest.loiNhan = $('#inp-message').val()
    }

    // hàm lấy dữ liệu lưu vào biến 
    function getDataOrderObj(paramOrderObj){
        paramOrderObj.menuStructure = gSelectedMenuStructure
        paramOrderObj.pizzaType = gSelectedPizzaType
        paramOrderObj.idLoaiNuocUong = $('#sel-drinks option:selected').val()
        paramOrderObj.hoTen = $.trim($('#inp-name').val())
        paramOrderObj.soDienThoai = $.trim($('#inp-phone').val())
        paramOrderObj.email = $.trim($('#inp-email').val())
        paramOrderObj.diaChi = $.trim($('#inp-address').val())
        paramOrderObj.idVourcher = $.trim($('#inp-voucherid').val())
        paramOrderObj.loiNhan = $.trim($('#inp-message').val())
    }

    // hàm kiểm tra điều kiện input
    function validataOrderObj(paramOrderObj){
        if(paramOrderObj.menuStructure === null){
            alert('!!! Chưa chọn Size Pizza !!!')
            return false
        }
        if(paramOrderObj.pizzaType === null){
            alert('!!! Chưa chọn Loại Pizza !!!')
            return false
        }
        if(paramOrderObj.idLoaiNuocUong === ''){
            alert('!!! Chưa chọn Loại Nước Uống !!!')
            return false
        }
        if(paramOrderObj.hoTen === ''){
            alert('!!! Chưa nhập Họ và Tên !!!')
            return false
        }
        if(paramOrderObj.email === ''){
            alert('!!! Chưa nhập Email !!!')
            return false
        }
        const vCHECKMAIL = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!vCHECKMAIL.test(paramOrderObj.email)){
            alert('!!! Email chưa hợp lệ !!!')
            return false
        }
        if(paramOrderObj.soDienThoai === ''){
            alert('!!! CHưa nhập Số Điện Thoại !!!')
            return false
        }
        if(paramOrderObj.diaChi === ''){
            alert('!!! Chưa nhập Địa Chỉ')
            return false
        }
        return true
    }

    // hàm load danh sách nước uống lên select
    function loadListDrinkToSelect(){
        var vUrlDrinks = "http://42.115.221.44:8080/devcamp-pizza365/drinks";
        $.ajax({
            url : vUrlDrinks,
            type : 'get',
            dataType : 'json',
            success : function(res){
                for( var bI = 0; bI < res.length; bI++){
                    var bSelDrinks = $('#sel-drinks')
                    $('<option>')
                        .text(res[bI].tenNuocUong)
                        .val(res[bI].maNuocUong)
                        .appendTo(bSelDrinks)
                }
            }
        })
    }
    
    // hàm đổi màu btn click
    function changeColorBtnClick(paramBtn){
        if(paramBtn == 'small'){
            $('#btn-size-s').attr('style', 'background-color: red;')
            $('#btn-size-m').attr('style', 'background-color: orange;')
            $('#btn-size-l').attr('style', 'background-color: orange;')
        }
        if(paramBtn == 'medium'){
            $('#btn-size-s').attr('style', 'background-color: orange;')
            $('#btn-size-m').attr('style', 'background-color: red;')
            $('#btn-size-l').attr('style', 'background-color: orange;')
        }
        if(paramBtn == 'large'){
            $('#btn-size-s').attr('style', 'background-color: orange;')
            $('#btn-size-m').attr('style', 'background-color: orange;')
            $('#btn-size-l').attr('style', 'background-color: red;')
        }

        if(paramBtn == 'ocean'){
            $('#btn-ocean').attr('style', 'background-color: red;')
            $('#btn-hawaii').attr('style', 'background-color: orange;')
            $('#btn-bacon').attr('style', 'background-color: orange;')
        }
        if(paramBtn == 'hawaii'){
            $('#btn-ocean').attr('style', 'background-color: orange;')
            $('#btn-hawaii').attr('style', 'background-color: red;')
            $('#btn-bacon').attr('style', 'background-color: orange;')
        }
        if(paramBtn == 'bacon'){
            $('#btn-ocean').attr('style', 'background-color: orange;')
            $('#btn-hawaii').attr('style', 'background-color: orange;')
            $('#btn-bacon').attr('style', 'background-color: red;')
        }
    }

})